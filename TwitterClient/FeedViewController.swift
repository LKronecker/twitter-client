//
//  FeedViewController.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-19.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import UIKit
import SideMenu

class FeedViewController: UIViewController, FeedProtocol, PostTweetProtocol {
    
    var userProfile = TCUserProfileViewController()
    let loginPage = TCLoginViewController()
    var feedCollectionViewController = FeedCollectionViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (TCDatabaseService.sharedInstance.retreiveUser() == nil) {
            self.presentLoginPage()
        }
        else {
            self.createFeed()
        }
        
        // create User Profile
        let userProfileNavigationController = UISideMenuNavigationController()
        userProfileNavigationController.leftSide = true
        
        let userProfile : TCUserProfileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userProfile") as UIViewController as! TCUserProfileViewController
        self.userProfile = userProfile
        userProfileNavigationController.pushViewController(self.userProfile, animated: false)
        
        SideMenuManager.menuLeftNavigationController = userProfileNavigationController
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        //Navbar buttons
        let profile = self.createButonWith(rect: CGRect(x:0, y: 0, width:40, height:40), image: (UIImage(named: "icon-user"))!, selector: #selector(self.presentUserProfile))
        
        let logout = self.createButonWith(rect: CGRect(x:40, y: 0, width:40, height:40), image: (UIImage(named: "logout"))!, selector: #selector(self.logoutUser))
        navigationItem.leftBarButtonItems = [profile, logout]

        self.navigationItem.rightBarButtonItem = self.createButonWith(rect: CGRect(x:0, y: 0, width:40, height:40), image: (UIImage(named: "tweetButton"))!, selector: #selector(FeedViewController.presentPostTweetPage))
        
        self.title = "Home"
    }
    
    // Add Child View Controller
    private func addViewControllerAsChildViewController(viewController: UIViewController) {
        addChildViewController(viewController)
        view.addSubview(viewController.view)
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
        
        //Add Constraints to view
        viewController.view.snp.makeConstraints { (make) -> Void in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top:(self.navigationController?.navigationBar.frame.height)! + 30, left:10, bottom:10, right:10))
            
        }
    }
    
    // MARK .- Feed Protocol
    
    func createFeed() {
        TCDatabaseService.sharedInstance.deleteTweetList()
        TCDataBroker.sharedInstance.getAllTweets {(NSDictionary, NSError) in
            if ((NSError) != nil) {
                self.presentLoginAlert(title: "Failure", body: "There's a problem with the internet, please verify your connection.")
            } else {
                //Persist to database
                TCDatabaseService.sharedInstance.persistTweetList(tweetList: NSDictionary?["twitts"] as! [TCTwittMappingModel])
                self.feedCollectionViewController = FeedCollectionViewController()
                //Feed Colletion View
                self.addViewControllerAsChildViewController(viewController: self.feedCollectionViewController)
            }
        }
    }
    
    // Mark .- PostTweetProtocol
    
    internal func postTweet(tweet: TCTwittStorageModel) {
        TCDatabaseService.sharedInstance.persistTweetList(tweetList: self.feedCollectionViewController.tweetList)
        self.feedCollectionViewController.tweetList.insert(tweet, at: 0)
        self.feedCollectionViewController.collectionView?.reloadData()
    }

    fileprivate func presentLoginPage() {
        let loginPage : TCLoginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginPage") as UIViewController as! TCLoginViewController
        loginPage.feedDelegate = self
        self.present(loginPage, animated: true, completion:nil)
    }
    
    func presentPostTweetPage() {
        let postTweetPage : TCPostTweetViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "posttweet") as UIViewController as! TCPostTweetViewController
        postTweetPage.postDelegate = self
        self.present(postTweetPage, animated: true, completion:nil)
    }
    
    func logoutUser()  {
        TCDatabaseService.sharedInstance.deleteUser()
        self.presentLoginPage()
    }
    
    func presentUserProfile() {
        self.feedCollectionViewController.collectionView?.setContentOffset(CGPoint(x:0, y:0), animated: false)
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    // Mark - Alert
    
    fileprivate func presentLoginAlert(title:String, body:String) -> Void {
        
        let alertController = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //UI
    func createButonWith(rect:CGRect, image:UIImage, selector:Selector) -> UIBarButtonItem {
        let button = UIButton()
        button.frame = rect
        button.setImage(image, for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0.3
        button.layer.borderColor = UIColor.black.cgColor
        
        let barButton = UIBarButtonItem()
        barButton.customView = button
        
        return barButton
    }
}

