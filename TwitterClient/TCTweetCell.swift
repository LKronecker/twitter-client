//
//  TCTweetCell.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-19.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//
import Foundation
import UIKit
class TCTweetCell: UICollectionViewCell {
    
    @IBOutlet var tweetAuthor: UILabel!
    @IBOutlet var tweetBody: UILabel!
    @IBOutlet var tweetImage: UIImageView!
    @IBOutlet var authorImage: UIImageView!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 0.3
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.masksToBounds = true;
    }
    
    override func prepareForReuse() {
        self.authorImage.image = UIImage(named:"paceholder")
    }
    
    public func update(tweet:TCTwittStorageModel){
        print("update")
        self.tweetAuthor.text! = tweet.author
        self.tweetBody.text! = tweet.body
        self.authorImage.af_setImage(withURL: NSURL(string:tweet.authorimage) as! URL)
    }
}
