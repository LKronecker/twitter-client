//
//  TCPostTweetViewController.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-21.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import UIKit

protocol PostTweetProtocol {
    func postTweet(tweet:TCTwittStorageModel)
}

class TCPostTweetViewController: UIViewController {
    weak var postDelegate : FeedViewController?
    @IBOutlet var tweetTextView: UITextView!
    @IBOutlet var postButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tweetTextView.becomeFirstResponder()
        
        self.styleButton(button: self.postButton)
        self.styleButton(button: self.cancelButton)
    }
    
    @IBAction func close(sender: UIButton) {
        self.closeView()
    }
    
    @IBAction func postTweet(sender: UIButton) {
        
        //IMPORTANT NOTE: Inside this method the posted tweet should also be updatd on the server through the API so that it can be reached by every user. In this app we dont do so since we have no complete server to update.
        if (self.tweetTextView.text! != "") {
            let user = TCDatabaseService.sharedInstance.retreiveUser()
            let tweet = TCTwittStorageModel()
            
            tweet.author = (user?.username)!
            tweet.authorimage = (user?.image)!
            tweet.body = self.tweetTextView.text!
        
            self.postDelegate?.postTweet(tweet: tweet)
            self.closeView()
        }
    }

    fileprivate func closeView() {
        self.dismiss(animated: true) {
        }
    }
    
    fileprivate func styleButton(button:UIButton) {
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.masksToBounds = true;
    }
}
