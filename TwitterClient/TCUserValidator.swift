//
//  TCUserValidator.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-21.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import UIKit

/////////////////////////////////////////////////////////////////////////////////////////////////////
// IMPORTANT NOTE: This validation should be done in the server since we should NOT fetch the entire list of users (they could be thousands).
// I will do validation here just for this specific case since we are emulating a service and we dont have a REST API and DB to communicate with.
//////////////////////////////////////////////////////////////////////////////////////////////////////


class TCUserValidator {
    
    func isUserValid(userList:[TCUserMappingModel], username:String, password:String) -> Bool {
        var userValidationList = [Bool]()
        for user in userList {
            if user.username == username && user.password == password {
                TCDatabaseService.sharedInstance.persistUser(user:user)
                userValidationList.append(true)
            }
            else {
                userValidationList.append(false)
            }
        }
        
        var finalFlag = false
        for flag in userValidationList {
            finalFlag = finalFlag || flag
            print(flag)
        }
        print(finalFlag)
        return finalFlag
    }
}
