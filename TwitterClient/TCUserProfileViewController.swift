//
//  TCUserProfileViewController.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-20.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import UIKit
import RealmSwift

class TCUserProfileViewController: UIViewController {
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var username: UILabel!
    @IBOutlet var useremail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.userImage.image = UIImage(named:"paceholder")
        self.update()
        
        self.userImage.layer.borderWidth = 3
        self.userImage.layer.masksToBounds = false
        self.userImage.layer.borderColor = UIColor.white.cgColor;
        self.userImage.layer.cornerRadius = self.userImage.frame.height/4
        self.userImage.clipsToBounds = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.userImage.image = UIImage(named:"paceholder")
    }

    fileprivate func update() {
        let user : TCUserStorageModel = TCDatabaseService.sharedInstance.retreiveUser()
        
        self.username.text = user.username
        self.useremail.text = user.email
        
        TCDataBroker.sharedInstance.getImage(imageString: user.image) { (NSDictionary, NSError) in
            if ((NSError) != nil) {
                print("Error fetching image!")
            } else {
                let image = NSDictionary?["image"]
                 self.userImage.image = image as! UIImage?
            }
        }
    }
}
