//
//  TCLoginViewController.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-19.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RealmSwift

protocol FeedProtocol {
    func createFeed()
}

class TCLoginViewController: UIViewController {
    
    weak var feedDelegate : FeedViewController?
    
    let closeButton = UIButton(type: UIButtonType.system) as UIButton
    let testLabel = UILabel()
    var userList = [TCUserMappingModel]()
    let realm = try! Realm()
    
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var twitterImageView: UIImageView!
    @IBOutlet var loginButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginButton.layer.cornerRadius = 5
        self.loginButton.layer.borderWidth = 0.5
        self.loginButton.layer.borderColor = UIColor.white.cgColor
        self.loginButton.layer.masksToBounds = true;
        self.usernameField.becomeFirstResponder()
        
        self.fetchUser()
    }
    
    // Mark - Actions
    
    @IBAction func login(sender: UIButton) {
        self.fetchUser()
        
        if (userList.count == 0){
            self.presentLoginAlert(title: "Failure", body: "There's a problem with the internet, please verify your connection.")
        }
        
        let userValidator = TCUserValidator()
        if userValidator.isUserValid(userList: self.userList, username: self.usernameField.text!, password: self.passwordField.text!) {
            self.dismiss(animated: true, completion: nil);
            self.feedDelegate?.createFeed()
        }
        else {
            self.presentLoginAlert(title: "Failure", body: "Your username or password are wrong. Please try again.")
        }
    }
    
    // User Fetching
    
    fileprivate func fetchUser() {
        TCDataBroker.sharedInstance.getAllUsers{(NSDictionary, NSError) in
            if ((NSError) != nil) {
                self.presentLoginAlert(title: "Failure", body: "There's a problem with the internet, please verify your connection.")
            } else {
                self.userList = NSDictionary?["users"] as! [TCUserMappingModel]
            }
        }
    }
        
    // Mark - Login Alert
    
    fileprivate func presentLoginAlert(title:String, body:String) -> Void {
        
        let alertController = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
