//
//  FeedCollectionViewController.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-19.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import UIKit
import RealmSwift

class FeedCollectionViewController: UICollectionViewController {
    
    var tweetList = [TCTwittStorageModel]()
    let tweetCellIdentifier = "tweetCellIdentifier"
    
    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
        self.collectionView?.register(UINib.init(nibName: "TCTweetCell", bundle: nil), forCellWithReuseIdentifier: tweetCellIdentifier)
    }
    
    convenience init() {
        let layout = UICollectionViewFlowLayout()
        self.init(collectionViewLayout: layout)
        self.tweetList = TCDatabaseService.sharedInstance.retreiveTweetList()
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView?.showsVerticalScrollIndicator = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView?.setContentOffset(CGPoint(x:0, y:0), animated: false)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tweetList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: tweetCellIdentifier, for: indexPath) as! TCTweetCell
        cell.update(tweet: self.tweetList[indexPath.row])
        return cell
    }

    //Mark: Flow Layout
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let collectionSize: CGRect = (self.collectionView?.frame)!
        
        return CGSize(width: collectionSize.width, height: 100);
        
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
}

