//
//  TCTwittBroker.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-19.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

typealias ServiceResponse = (NSDictionary?, NSError?) -> Void

class TCDataBroker: NSObject {
    
    let BASE_URL = "http://localhost:3000/"
    
    class var sharedInstance:TCDataBroker {
        struct Singleton {
            static let instance = TCDataBroker()
        }
        return Singleton.instance
    }
    
    func getAllTweets(onCompletion: @escaping ServiceResponse){
        let URL = String(format: "%@%@", self.BASE_URL, "twitts")
        Alamofire.request(URL).responseArray { (response: DataResponse<[TCTwittMappingModel]>) in
            if (response.result.value != nil) {
                let responseDict = ["twitts":response.result.value!] as NSDictionary
                onCompletion(responseDict, nil)
            }
            else {
                onCompletion(nil, NSError())
            }
        }
    }
    
    func getAllUsers(onCompletion: @escaping ServiceResponse){
        let URL = String(format: "%@%@", self.BASE_URL, "users")
        Alamofire.request(URL).responseArray { (response: DataResponse<[TCUserMappingModel]>) in
            
            if (response.result.value != nil) {
                let responseDict = ["users":response.result.value!] as NSDictionary
                onCompletion(responseDict, nil)
            }
            else {
                onCompletion(nil, NSError())
            }
        }
    }
    
    func getImage(imageString:String, onCompletion: @escaping ServiceResponse){
        Alamofire.request(imageString).responseImage { response in
            
            if let image = response.result.value {
                let responseDict = ["image":response.result.value!] as NSDictionary
                onCompletion(responseDict, nil)
                print("image downloaded: \(image)")
            }
            else {
                onCompletion(nil, NSError())
            }
        }
    }
}
