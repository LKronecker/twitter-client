//
//  TCUserModel.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-19.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import ObjectMapper

class TCUserMappingModel: Mappable {
    dynamic var id: String? = nil
    dynamic var name: String? = nil
    dynamic var password: String? = nil
    dynamic var username : String? = nil
    dynamic var email: String? = nil
    dynamic var image : String? = nil
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        password <- map["password"]
        username <- map["username"]
        email <- map["email"]
        image <- map["image"]
    }
}

import RealmSwift

class TCUserStorageModel: Object {
    
    dynamic var id = " "
    dynamic var name = " "
    dynamic var password = " "
    dynamic var username = " "
    dynamic var email = " "
    dynamic var image = " "
}
