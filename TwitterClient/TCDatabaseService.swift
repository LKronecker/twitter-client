//
//  TCDataStore.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-21.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import Foundation
import RealmSwift

class TCDatabaseService {
    
    class var sharedInstance:TCDatabaseService {
        struct Singleton {
            static let instance = TCDatabaseService()
        }
        return Singleton.instance
    }

    let realm = try! Realm()
    
    //User
    func retreiveUser () -> TCUserStorageModel! {
        let userList = realm.objects(TCUserStorageModel.self)
        if (userList.count > 0) {
            return userList[0]
        }
    return nil
    }
    
    func persistUser (user:TCUserMappingModel) {
        let userToStore = TCUserStorageModel()
        userToStore.name = user.name!
        userToStore.username = user.username!
        userToStore.email = user.email!
        userToStore.id = user.id!
        userToStore.password = user.password!
        userToStore.image = user.image!
        
        try! realm.write {
            realm.add(userToStore)
        }
    }
    
    func deleteUser() {
        try! realm.write {
            realm.delete(realm.objects(TCUserStorageModel.self))
        }
    }
    
    //Tweet
    var savedTweetList : Results<TCTwittStorageModel>!
    
    func persistTweet(tweet:TCTwittStorageModel) {
        try! realm.write {
            realm.add(tweet)
        }
    }
    
    fileprivate func persistTweetMapping (tweet:TCTwittMappingModel) {
        let tweetToStore = TCTwittStorageModel()
        tweetToStore.id = tweet.id!
        tweetToStore.title = tweet.title!
        tweetToStore.body = tweet.body!
        tweetToStore.authorimage = tweet.authorimage!
        tweetToStore.author = tweet.author!
        
        self.persistTweet(tweet: tweetToStore)
    }
    
    func persistTweetList(tweetList:[AnyObject]) {
        if tweetList is [TCTwittMappingModel]  {
            for tweet in tweetList {
                self.persistTweetMapping(tweet: tweet as! TCTwittMappingModel)
            }
        }
        else {
            for tweet in tweetList {
                self.persistTweet(tweet: tweet as! TCTwittStorageModel)
            }
        }
    }
    
    func retreiveTweetList () -> [TCTwittStorageModel] {
        return Array(realm.objects(TCTwittStorageModel.self))
    }
    
    func deleteTweetList () {
        try! realm.write {
            realm.delete(realm.objects(TCTwittStorageModel.self))
        }
    }
}
