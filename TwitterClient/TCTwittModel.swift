//
//  TCTwittModel.swift
//  TwitterClient
//
//  Created by Leopoldo Garcia Vargas on 2016-09-19.
//  Copyright © 2016 Leopoldo Garcia Vargas. All rights reserved.
//

import ObjectMapper

class TCTwittMappingModel: Mappable {
    var id: String! = nil
    var title: String! = nil
    var body: String? = nil
    var author: String? = nil
    var authorimage: String? = nil
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["_id"]
        title <- map["title"]
        body <- map["body"]
        author <- map["author"]
        authorimage <- map["authorimage"]
    }
}

import RealmSwift

class TCTwittStorageModel: Object {
    dynamic var id = " "
    dynamic var title = ""
    dynamic var body = " "
    dynamic var author = " "
    dynamic var image = " "
    dynamic var authorimage = " "
}
