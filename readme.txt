This project is a Twitter client written in Swift 3.0 on Xcode 8.0.

The App is connected to a local json-server which provides data and allows us to test the app in a more real life scenario . To run the app successfully you will need to globally instal the node package by typing:

npm install -g json-server

Then start the server by going to: ‘... /TwitterClient/json-server’ in the terminal and typing:

 json-server --watch db.json

The file db.json contains the data that will be used to populate the application.

TO LOGIN TO THE APP USE THE FOLLOWING CREDENTIALS:

Username: Gandalf
Password: trewq

or

Username: JHolmes
Password: qwert

Architecture.

The application makes use of different patterns to achieve it’s goals. 
MVC: Used to distribute work in the feed collection view controller. The cell is be the view, the Feed model the model and the collection view controller would be the controller.
Delegate/Protocol: To establish communication between several VCs without creating direct dependencies.
Singleton service providers: the datastore and data broker are Singleton objects that are accessible through all the app.
View controller containment: The Feed view contains the feed collection view controller as a child to delegate responsibilities properly and to distribute code more clearly.


Login/Logout

When the user tries to login, the app  validates by fetching a list of users that are registered in the server and tries to find the user that’s trying to log in in the list. If found the login request is accepted and the user is taken to the Twitter Feed. This validation process should be done on the server side so that the app just needs to send the user’s username and password to the server and gets a response that specifies if the user is allowed in or not. This is being done by the app at this time since I did not have an API with such functionality accessible.

After this the user is persisted in a Real database so that next time that the app is opened there is no need to log in anymore. It also allows us to access the user’s data directly to populate the user Profile section.

When the user logs out the database is cleared for any stored user data, at the same time the login page is presented again.

Tweet Feed

Once the user is allowed in by the validation process the Feed page is presented. As the login view is dismissed the app clears the database for any recently stored tweet and fetches the new most recent tweets, this with the intention of always fetching the newest content as the user logs in. After successfully fetched they are stored in the Realm database and the Feed collection view is created to display the tweet data.

Post Tweet

As the user posts a tweet a new model is created and appended to the tweet list in the database. Once the datasource is updated the collection view is refreshed to show the newly added cell. At this point the new tweet should also be posted to the server. This functionality is not implemented since there’s no backend to support it.
